<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Борисенков Сергей 211-323</title>

  <link rel="stylesheet" href="style.css">
</head>
<body>
  <header class="header0">
    <div class="header1">
      <img src="mospol.jpg" alt="mospol">
    </div>
    <div>
    <p>Задание для самостоятельной работы «Hello, World!»</p>
    </div>
  </header>
  <main>
  <p><?php echo 'Hello, World!';?></p>
  </main>
  <footer class="footer">
  <p>Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг. 
Дано:
Header = слева логотип МосПолитеха, по центру название работы.
Footer = задание для самостоятельно работы (без описания).
Main = любой html-элемент с адекватным динамическим контентом (пример Hello, World).
Ответ на гугл диск:
Ссылка на репозиторий
URL – адрес страницы.
</p>
  </footer>
</body>
</html>
